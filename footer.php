    <!-- RODAPE -->
    <footer class="rodape">
        <div class="chamada-whatsapp">
            <a href="https://api.whatsapp.com/send?phone=5581982174970&text=Olá, estou entrando em contato através do site AmigoDr.">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
            </a>
        </div>
        <div class="container">
            <!-- FORMAS DE PAGAMENTO -->
            <div class="coluna">
                <p class="titulo">Formas de pagamento</p>
                <div class="info">
                    <p class="info-nome">Você pode realizar seus pagamentos em até 6x no cartão de crédito ou com desconto para pagamento em débito ou dinheiro. Escolha a melhor forma para você.</p>
                    <p class="nome-destaque">Crédito</p>
                    <p class="info-nome">Online e Centro Médico</p>
                    <div class="bandeiras">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/1.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/2.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/3.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/4.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/5.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/6.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/7.png">
                    </div>
                </div>
                <div class="info">
                    <p class="nome-destaque">Débito</p>
                    <p class="info-nome">Apenas no centro médico</p>
                    <div class="bandeiras">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/1.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/2.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/3.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/4.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/5.png">
                    </div>
                </div>
                <div class="info">
                    <p class="nome-destaque">Outras formas</p>
                    <p class="info-nome">Apenas no centro médico</p>
                    <div class="bandeiras">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/8.png">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento/9.png">
                    </div>
                </div>
            </div>

            <!-- MAPA DO SITE -->
            <div class="coluna">
                <p class="titulo">Mapa do site</p>
                <div class="info">
                    <p class="nome-destaque"><a href="agende">Agende</a> - <a href="consultas">Consultas</a> - <a href="exames">Exames</a> - <a href="exames#check-up">Check-ups</a> - <a href="sobre">Sobre</a> - <a href="#">Localização</a> - <a href="medicos">Médicos</a> - <a href="trabalhe-conosco">Trabalhe Conosco</a> - <a href="ajuda">Ajuda</a> - <a href="contato">Contato</a></p>
                </div>
                <div class="info">
                    <p class="nome-destaque">Não atendemos emergência</p>
                    <p class="info-nome">Responsável técnico: Dr. Reis Massafelli Gonçalves - Médico - CRM-SP 143.601</p>
                </div>
                <div class="info info-contato">
                    <div class="email">
                        <p class="nome-destaque">Atendimento ao cliente</p>
                        <p class="info-nome">atendimento@amigodr.com</p>
                    </div>
                    <div class="telefone">
                        <p class="nome-destaque">Telefone</p>
                        <a href="tel:81982174970" class="info-nome">(81) 9 8217-4970</a>
                    </div>
                </div>
            </div>

            <!-- REDES SOCIAIS -->
            <div class="coluna redes-sociais">
                <p class="titulo">Redes Sociais</p>
                <div class="div">
                    <a href="https://www.instagram.com/amigodr.clinica/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.png"></a>
                    <a href="https://www.facebook.com/amigodr.clinica/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook.png"></a>
                    <a href="https://www.youtube.com/channel/UC5ff8LGPirnHvLh3tWfF2ag" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/youtube.png"></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- ASSINATURA -->
    <div class="assinatura">
        <div class="container">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/lean.png">
        </div>
    </div>
    
    
        
    <!-- CHAMA O JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

    <!-- JS SCROLL SUAVE -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll-suave.js"></script>

    <!-- PREVIA SERVICOS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/previa-servicos.js"></script>

    <!-- AGENDE -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/select-agende.js"></script>

    <!-- CHAMA E INICIA O WOW.JS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>