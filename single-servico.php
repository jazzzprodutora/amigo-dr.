<?php
    // Template Name: Single Servico
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- SINGLE SERVICO -->
    <div class="single-servico">
        <div class="container">
            <!-- TITULO SERVICO -->
            <div class="titulo-single-servicos">
                <h1 class="titulo">Cardiologia<span>.</span></h1>
            </div>
            
            <!-- INFO SERVICO -->
            <div class="info">
                <div class="img-servico">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/single-servico.png">
                </div>
                <div class="coluna">
                    <div class="texto-servico">
                        <p class="texto">A cardiologia é uma das especialidades médicas mais evoluidas. É a cardiologia que cuida do diagnóstico e tratamento das doenças que acometem o coração, bem como os outros componentes do sistema circulatório. O médico especialista nessa área é o cardiologista.</p>
                    </div>
                    <div class="preco-servico">
                        <div class="valor">
                            <p class="dividido"><span>4x de R$30</span></p>
                            <p class="a-vista">ou R$120</p>
                        </div>
                        <div class="area-botao">
                            <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>