<?php
    // Template Name: Sobre
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- SOBRE -->
    <div class="sobre">
        <div class="container">

            <!-- TEXTO SOBRE -->
            <div class="texto-sobre">
                <h1 class="titulo">Sobre<span>.</span></h1>
                <p class="texto"><?php the_field('um-pouco-sobre'); ?></p>
            </div>

            <!-- HISTORIA -->
            <div class="historia">
                <div class="img-historia">
                    <img src="<?php the_field('imagem-historia'); ?>">
                </div>
                <div class="texto-historia">
                    <h1 class="titulo">História<span>.</span></h1>
                    <p class="texto"><?php the_field('texto-historia'); ?></p>
                </div>
            </div>

            <!-- IMG DESTAQUE -->
            <div class="img-destaque">
                <img src="<?php the_field('imagem-destaque'); ?>">
            </div>

            <!-- PILARES -->
            <div class="pilares">
                <!-- LOOP -->
                <?php if(have_rows('pilares')): while(have_rows('pilares')) : the_row(); ?>
                <div class="pilar">
                    <h1 class="titulo"><?php the_sub_field('titulo'); ?><span>.</span></h1>
                    <p class="texto"><?php the_sub_field('texto'); ?></p>
                </div>
                <?php endwhile; else : endif; ?>
            </div>
        </div>

        <!-- EQUIPE -->
        <!-- <div class="equipe">
            <div class="container">
                <h1 class="titulo">Equipe<span>.</span></h1>
                <div class="itens">

                    LOOP
                    <?php if(have_rows('equipe')): while(have_rows('equipe')) : the_row(); ?>
	
                        <div class="item">
                            <div class="img-membro">
                                <img src="<?php the_sub_field('imagem-membro'); ?>">
                            </div>
                            <div class="info-membro">
                                <h1 class="nome-membro"><?php the_sub_field('nome-membro'); ?></h1>
                                <p class="funcao-membro"><?php the_sub_field('funcao-membro'); ?></p>
                                <p class="bio-membro"><?php the_sub_field('bio-membro'); ?></p>
                            </div>
                        </div>

                    <?php endwhile; else : endif; ?>
                    ...
                </div>
            </div>
        </div> -->
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>