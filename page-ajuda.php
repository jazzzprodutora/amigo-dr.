<?php
    // Template Name: Ajuda
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- AJUDA -->
    <div class="ajuda">
        <div class="container">
            <!-- TEXTO AJUDA -->
            <div class="texto-ajuda">
                <h1 class="titulo">Central de Ajuda<span>.</span></h1>
                <div class="texto"><?php the_field('texto-ajuda'); ?></div>
            </div>

            <!-- PERGUNTAS FREQUENTES -->
            <div class="perguntas-frequentes">
                <h1 class="titulo">Perguntas Frequentes:</h1>

                <!-- ACCORDION -->
                <div class="accordion" id="accordionPerguntasFrequentes">

                    <!-- LOOP -->
                    <?php if(have_rows('perguntas-frequentes')): while(have_rows('perguntas-frequentes')) : the_row(); ?>
                    <div class="card">
                        <div class="card-header" id="">
                            <h2 class="mb-0">
                                <!-- PERGUNTA -->
                                <a class="btn btn-link" data-toggle="collapse" data-target="#pergunta<?php the_sub_field('id'); ?>" aria-expanded="true" aria-controls="pergunta<?php the_sub_field('id'); ?>">
                                    <p><?php the_sub_field('pergunta'); ?></p>
                                </a>
                            </h2>
                        </div>

                        <div id="pergunta<?php the_sub_field('id'); ?>" class="collapse show" aria-labelledby="" data-parent="#accordionPerguntasFrequentes">
                            <div class="card-body">
                                <p><?php the_sub_field('resposta'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; else : endif; ?>
                    <!-- ... -->

                </div>
            </div>

            <!-- FORMULARIO PERGUNTAS FREQUENTES -->
            <div class="formulario">
                <div class="campos-formulario">
                    <p class="texto-efeito">Entre em contato</p>
                    <h1 class="titulo">Ouvir é coisa de amigo</h1>
                    <?php echo do_shortcode( '[contact-form-7 id="167" title="Contato"]' ); ?>
                    <!-- <p class="texto">Anexe seu currículo e boa sorte.</p> -->

                    <!-- <form action="">
                        <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/usuario-mini.png">
                            <input type="text" class="form-control" placeholder="Nome">
                        </div>
                        <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mensagem-mini.png">
                            <input type="email" class="form-control" placeholder="E-mail">
                        </div>
                        <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/usuario-mini.png">
                            <input type="fone" class="form-control" placeholder="Telefone">
                        </div>
                        <textarea name="" id="mensagem" class="form-control" cols="30" rows="10" placeholder="Mensagem"></textarea>
                        <div class="area-botao">
                            <button class="botao botao-principal">Enviar</button>
                        </div>
                    </form> -->
                </div>
                <div class="img-formulario">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/perguntas-frequentes/mensagem.png">
                    <p>Ouvir é coisa de amigo. Por isso, queremos ouvir você. Entre em contato, fale com a gente. Estamos sempre dispostos a lhe ouvir e tirar todas as suas dúvidas.</p>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>