<?php
    // Template Name: Medicos
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- MEDICOS -->
    <div class="medicos">
        <div class="container">
            <div class="info-medicos">
                <h1 class="titulo">Médicos<span>.</span></h1>
                <p class="texto">Veja aqui nosso corpo médico. O amigodr trabalha exclusivamente com grandes especialistas da medicina. Porque nada é mais importante do que você estar com a sua saúde sempre em dia.</p>
            </div>
            <div class="itens">
                
                <!-- LOOP -->
                <?php if(have_rows('medicos')): while(have_rows('medicos')) : the_row(); ?>
	
                    <div class="item">
                        <div class="img-medico">
                            <img src="<?php the_sub_field('imagem-medico'); ?>">
                        </div>
                        <div class="info-medico">
                            <h2 class="h4 nome-medico"><?php the_sub_field('nome-medico'); ?></h2>
                            <p class="funcao-medico"><?php the_sub_field('funcao-medico'); ?></p>
                        </div>
                    </div>

                <?php endwhile; else : endif; ?>
                <!-- ... -->
                
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>