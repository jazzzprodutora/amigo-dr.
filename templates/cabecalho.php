<!-- CABECALHO -->
<div class="cabecalho">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/logo.png" alt="Amigo Dr."></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto">
                <a class="nav-item nav-link" href="agende">Agende</a>

                <!-- DROPDOWN -->
                <div class="dropdown">
                    <a class="dropdown-toggle nav-item nav-link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Serviços</a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="consultas">Consultas</a>
                        <a class="dropdown-item" href="exames">Exames</a>
                        <a class="dropdown-item scroll" href="exames#check-up">Check-ups</a>
                        <a class="dropdown-item" href="outros">Outros</a>
                    </div>
                </div>

                <a class="nav-item nav-link" href="sobre">Sobre</a>
                <a class="nav-item nav-link" href="home#localizacao">Localização</a>
                <!-- <a class="nav-item nav-link" href="medicos">Médicos</a> -->
                <a class="nav-item nav-link" href="trabalhe-conosco">Trabalhe Conosco</a>
                <a class="nav-item nav-link" href="ajuda">Ajuda</a>
                <a class="nav-item nav-link" href="contato">Contato</a>
            </div>
        </div>
    </nav>
</div>