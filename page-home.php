<?php
    // Template Name: Home
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>

            <!-- BANNER -->
            <div class="banner">
                <div id="carouselBanner" class="carousel slide" data-ride="carousel" data-interval="false">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselBanner" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselBanner" data-slide-to="1"></li>
                        <li data-target="#carouselBanner" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- LOOP -->
                        <?php if(have_rows('carouselHome')): while(have_rows('carouselHome')) : the_row(); ?>
                        <div class="carousel-item">
                            <a href="<?php the_sub_field('link'); ?>">
                                <div class="item">
                                    <div class="imagem-e">
                                        <img src="<?php the_sub_field('imagem-esquerda'); ?>">
                                    </div>
                                    <div class="imagem-d">
                                        <img src="<?php the_sub_field('imagem-direita'); ?>">
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php endwhile; else : endif; ?>
                        <?php  wp_reset_postdata();?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselBanner" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselBanner" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Proximo</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- INFO CARDS -->
    <div class="info-cards">
        <div class="container">
            <div class="cards">
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/calendario.png" alt="Agende">
                    <h2 class="titulo">Agende até para amanhã</h2>
                    <p class="texto">Felicidade é coisa de amigo. Por isso, o amigodr. permite marcação de consultas até de um dia para outro.</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pagamento.png" alt="Pagamento">
                    <h2 class="titulo">Formas de pagamento</h2>
                    <p class="texto">Você pode realizar seus pagamentos ao amigodr. através de cartão de débito, crédito e em espécie. Escolha a melhor forma para você.</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/localizacao.png" alt="Localizacao">
                    <h2 class="titulo">Localização</h2>
                    <p class="texto">A Clínica amigodr. fica localizada na Rua Uriel de Holanda, 756, Linha do Tiro - Recife/PE - CEP: 52.131-150 - 81 3034.1916</p>
                </div>
            </div>
        </div>
    </div>

    <!-- CHAMADA -->
    <div class="texto-chamada">
        <div class="container">
            <h3 class="frase">A confiança de ter um grande amigo sempre que você precisar.</h3>
        </div>
    </div>

    <!-- PREVIA SERVICOS -->
    <div class="previa-servicos">
        <div class="container">
            <!-- PIILS -->
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-consultas-tab" data-toggle="pill" href="#pills-consultas" role="tab" aria-controls="pills-consultas" aria-selected="true">Consultas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-exames-tab" data-toggle="pill" href="#pills-exames" role="tab" aria-controls="pills-exames" aria-selected="false">Exames</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-checkups-tab" data-toggle="pill" href="#pills-checkups" role="tab" aria-controls="pills-checkups" aria-selected="false">Check-ups</a>
                </li>
            </ul>
            <div class="tab-content" id="pillsServicos">
                <div class="tab-pane fade show active" id="pills-consultas" role="tabpanel" aria-labelledby="pills-consultas-tab">
                
                    <!-- CONSULTAS -->
                    <div class="cards">
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'consultas'
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>
                        <?php endwhile; else: endif; ?>
                        <?php  wp_reset_postdata();?>
                    </div>

                    <div class="area-botao-ver-mais">
                        <div class="divisor"></div>
                            <a href="consultas"><button class="botao botao-principal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png">Mais Consultas<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png"></button></a>
                        <div class="divisor"></div>
                    </div>

                </div>

                <div class="tab-pane fade" id="pills-exames" role="tabpanel" aria-labelledby="pills-exames-tab">

                    <!-- EXAMES -->
                    <div class="cards">
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'exames'
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>
                        <?php endwhile; else: endif; ?>
                        <?php  wp_reset_postdata();?>
                    </div>

                    <div class="area-botao-ver-mais">
                        <div class="divisor"></div>
                            <a href="exames"><button class="botao botao-principal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png">Mais Exames<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png"></button></a>
                        <div class="divisor"></div>
                    </div>

                </div>
                <div class="tab-pane fade" id="pills-checkups" role="tabpanel" aria-labelledby="pills-checkups-tab">
                    
                    <!-- CHECK-UPS -->
                    <div class="cards">
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'check-ups'
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>
                        
                        <?php endwhile; else: endif; ?>
                        <?php  wp_reset_postdata();?>
                    </div>

                    <div class="area-botao-ver-mais">
                        <div class="divisor"></div>
                            <a href="exames#check-up"><button class="botao botao-principal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png">Mais Check-ups<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mais-botao.png"></button></a>
                        <div class="divisor"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- DEPOIMENTOS -->
    <div class="depoimentos">
        <div class="container">
            <!-- LOOP -->
            <?php if(have_rows('depoimentos')): while(have_rows('depoimentos')) : the_row(); ?>
            <div class="depoimento">
                <div class="texto">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/abre-aspas.png">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/fecha-aspas.png">
                    <p><?php the_sub_field('texto-depoimento'); ?></p>
                </div>
                <div class="info">
                    <div class="img-autor">
                        <img src="<?php the_sub_field('imagem-autor'); ?>">
                    </div>
                    <div class="info-autor">
                        <p class="nome"><?php the_sub_field('nome-autor'); ?></p>
                        <p class="cargo"><?php the_sub_field('funcao-autor'); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile; else : endif; ?>
            <?php  wp_reset_postdata();?>
            <!-- ... -->
        </div>
    </div>

    <!-- NEWS (PLUGIN INSTAGRAM) -->
    <div class="news">
        <div class="container">
            <h2 class="titulo">Novidades<span>.</span></h2>
            <?php echo do_shortcode( '[instagram-feed]' ); ?>
        </div>
    </div>

    <!-- UNIDADES -->
    <div class="unidades">
        <div class="container">
            <div class="item">
                <div class="img-unidade">
                    <img src="<?php the_field('imagem-unidade1'); ?>" alt="<?php the_field('nome-unidade1'); ?>">
                    <div class="nome-unidade">
                        <p><?php the_field('nome-unidade1'); ?></p>
                    </div>
                </div>
                <div class="texto-unidade">
                    <p><?php the_field('texto-unidade1'); ?></p>
                </div>
            </div>
            <div class="item">
                <div class="img-unidade">
                    <img src="<?php the_field('imagem-unidade2'); ?>" alt="<?php the_field('nome-unidade2'); ?>">
                    <div class="nome-unidade">
                        <p><?php the_field('nome-unidade2'); ?></p>
                    </div>
                </div>
                <div class="texto-unidade">
                    <p><?php the_field('texto-unidade2'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <!-- MAPA -->
    <div class="localizacao" id="localizacao">
        <div class="itens">
            <div class="item">
                <h3 class="titulo-unidade">- Linha do Tiro</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.914531613549!2d-34.90535398511511!3d-8.007750694233211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab182d9a7485c5%3A0x6274ce5cc6d51c45!2sR.%20Uriel%20de%20Holanda%2C%20756%20-%20Linha%20do%20Tiro%2C%20Recife%20-%20PE%2C%2052131-150!5e0!3m2!1spt-BR!2sbr!4v1606759204344!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="item">
                <h3 class="titulo-unidade">- Santo Amaro</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.5300666146086!2d-34.89047028511488!3d-8.047286394205548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab18ed8a2e9ec3%3A0x5fdb04b6f3eee06d!2sR.%20Arn%C3%B3bio%20Marqu%C3%AAs%2C%20253%20-%20Santo%20Amaro%2C%20Recife%20-%20PE%2C%2050100-130!5e0!3m2!1spt-BR!2sbr!4v1606759271665!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>