    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>

    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>

    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>

    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>