<?php
    // Template Name: Exames
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- EXAMES -->
    <div class="exames">
        <div class="container">
            <!-- TEXTO EXAMES -->
            <div class="texto-exames">
                <h1 class="titulo">Exames<span>.</span></h1>
                <div class="texto"><?php the_field('texto-exames'); ?></div>
            </div>
            <!-- LISTA EXAMES -->
            <div class="lista-exames">
                <div class="container">
                    <div class="cards">
                        <!-- Havoc-->
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'exames',
                                'posts_per_page'=> -1
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>

                        <?php endwhile; else: endif; ?>

                        <!-- LOOP -->
                        <!-- <?php // if(have_rows('exames')): while(have_rows('exames')) : the_row(); ?>
                        <div class="item">
                            <img src="<?php the_sub_field('imagem-exame'); ?>">
                            <h2 class="titulo"><?php the_sub_field('titulo-exame'); ?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_sub_field('valor-dividido'); ?></span></p>
                                <p class="a-vista">ou <?php the_sub_field('valor-integral'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>
                        <?php //endwhile; else : endif; ?> -->
                        <!-- ... -->
                    </div>
                </div>
            </div>
        </div>
        <!-- CHECK-UP -->
        <div class="check-up" id="check-up">
            <div class="container">
                <!-- TEXTO CHECK-UP -->
                <div class="texto-check-up">
                    <h1 class="titulo">Check-up<span>.</span></h1>
                    <p class="texto">O amigodr possui uma enorme quantidade de exames e serviços para cuidar de você por completo. Confira aqui a relação completa e os preços correspondentes.</p>
                </div>
                <!-- LISTA EXAMES -->
                <div class="lista-exames lista-check-up">
                    <div class="container">
                        <div class="cards">
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'Check-ups',
                                'posts_per_page'=> -1
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>

                        <?php endwhile; else: endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>