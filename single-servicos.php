<?php
    // Template Name: Single Servico
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- SINGLE SERVICO -->
    <div class="single-servico">
        <div class="container">
            <!-- TITULO SERVICO -->
            <div class="titulo-single-servicos">
                <h1 class="titulo"><?php the_title()?><span>.</span></h1>
            </div>
            
            <!-- INFO SERVICO -->
            <div class="info">
                <div class="img-servico">
                    <?php the_post_thumbnail()?>
                </div>
                <div class="coluna">
                    <div class="texto-servico">
                        <p class="texto"><?php the_field('conteudo_servico')?></p>
                    </div>
                    <div class="preco-servico">
                        <div class="valor">
                            <p class="dividido"><span><?php the_field('divisao')?></span></p>
                            <p class="a-vista">ou <?php the_field('valor-total')?></p>
                        </div>
                        <div class="area-botao">
                            <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>