<?php
    // Template Name: Agende
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- AGENDE -->
    <div class="agende">
        <div class="container">
            <!-- TEXTO AGENDE -->
            <div class="texto-agende">
                <h1 class="titulo">Solicite Agendamento<span>.</span></h1>
                <p class="texto">Solicite o agendamento de sua consulta facilmente por aqui mesmo. É só preencher os dados no campo ao lado. Você também pode ligar no (81) 9 8217-4970, falar pelo nosso <a href="https://api.whatsapp.com/send?phone=81982174970&text=" target="_blank">Whatsapp</a> ou via email: <a href="mailto:atendimento@amigodr.com">atendimento@amigodr.com</a></p>
            </div>

            <!-- FORMULARIO TRABALHE CONOSCO -->
            <div class="formulario">
                <div class="img-formulario">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/agende/imagem-form.png">
                </div>

                <div class="campos-formulario">
                    <select class="form-control" name="categoria" id="categoria">
                        <option value="default">Categoria</option>
                        <option value="consultas">Consultas</option>
                        <option value="exames">Exames</option>
                        <option value="check-ups">Check-ups</option>
                        <option value="outros">Outros</option>
                    </select>

                    <div class="linha">
                        <div class="item">
                            <!-- AREA DO SELECT ESPECIALIDADES-->
                            <div class="area-lista-especialidades"></div>
                        </div>

                        <div class="item">
                            <select class="form-control" name="unidade" id="unidade">
                                <option value="default">Unidade</option>
                                <option value="linha-do-tiro">Linha do Tiro</option>
                                <option value="santo-amaro">Santo Amaro</option>
                            </select>
                        </div>
                    </div>

                    <div class="linha">
                        <div class="item">
                            <div class="data">
                                <p class="texto">Data:</p>
                                <input type="date" class="form-control" name="data" id="">
                            </div>
                        </div>

                        <div class="item">
                            <div class="horario">
                            <select class="form-control" name="horario" id="horario">
                                <option value="">Horário</option>
                                <option value="Das 8h às 12h">Das 8h às 12h</option>
                                <option value="Das 14h às 18h">Das 14h às 18h</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input type="text" class="form-control mt-4" placeholder="Nome" name="nome">
                <input type="email" class="form-control mt-4" placeholder="E-mail" name="email">
                <input type="fone" class="form-control mt-4" placeholder="Telefone" name="telefone">
                <div class="alinhar">
                    <div class="area-botao">
                    <button type="submit" class="botao botao-principal">Agendar</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>