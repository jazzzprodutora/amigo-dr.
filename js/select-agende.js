var categoria = document.querySelector('#categoria');
var areaListaEspecialidades = document.querySelector(
  '.area-lista-especialidades',
);
categoria.addEventListener('change', () => {
  areaListaEspecialidades.innerHTML = categoria.value;
  if (categoria.value === 'consultas') {
    areaListaEspecialidades.innerHTML = `
        <select class="form-control" name="especialidade" id="especialidade">
            <option value="default">Especialidade</option>
            <option value="cardiologia">Cardiologia</option>
            <option value="clinica-geral">Clínica Geral</option>
            <option value="dermatologia">Dermatologia</option>
            <option value="endocrinologia">Endocrinologia</option>
            <option value="gastroenterologia">Gastroenterologia</option>
            <option value="geriatria">Geriatria</option>
            <option value="ginecologia">Ginecologia</option>
            <option value="pediatria">Pediatria</option>
            <option value="pneumologia">Pneumologia</option>
            <option value="psicologia">Psicologia</option>
            <option value="urologia">Urologia</option>
        </select>`;
    areaListaEspecialidades.classList.remove('estilo-especialidades');
  } else if (categoria.value === 'exames') {
    areaListaEspecialidades.innerHTML = `
    <select class="form-control" name="exames" id="exames">
        <option value="default">Especialidade</option>
        <option value="colonoscopia">Colonoscopia</option>
        <option value="ecocardiograma">Ecocardiograma</option>
        <option value="eletrocardiograma">Eletrocardiograma (ECG)</option>
        <option value="endoscopia-digestiva">Endoscopia digestiva</option>
        <option value="ergometrico">Ergométrico</option>
        <option value="exames-laboratoriais">Exames Laboratoriais</option>
        <option value="holter">Holter</option>
        <option value="mamografia-digial">Mamografia Digial</option>
        <option value="mapa">MAPA</option>
        <option value="radiografia(raio-x)">Radiografia(raio-x)</option>
        <option value="ressonancia-magnetica">Ressonância Magnetica</option>
        <option value="tomografia-computadorizada">Tomografia Computadorizada</option>
        <option value="ultrassom">Ultrassom</option>
        <option value="ultrassom-com-doppler">Ultrassom com Doppler</option>
        <option value="phmetria">Phmetria</option>
        <option value="manometria">Manometria</option>
        <option value="estomias(tratamento-de-lesoes)">Estomias (tratamento de lesões)</option>
    </select>`;
    areaListaEspecialidades.classList.remove('estilo-especialidades');
  } else if (categoria.value === 'check-ups') {
    areaListaEspecialidades.innerHTML = `
    <select class="form-control" name="exames" id="exames">
        <option value="default">Especialidade</option>
        <option value="cardiologico">Cardiológico</option>
        <option value="da-mulher">Da Mulher</option>
        <option value="do-homem">Do Homem</option>
        <option value="do(a)-idoso(a)">Do(a) Idoso(a)</option>
    </select>`;
    areaListaEspecialidades.classList.remove('estilo-especialidades');
  } else if (categoria.value === 'outros') {
    areaListaEspecialidades.innerHTML = `
    <select class="form-control" name="exames" id="exames">
        <option value="default">Especialidade</option>
        <option value="medicina-ocupacional">Medicina Ocupacional</option>
        <option value="fisioterapia">Fisioterapia</option>
        <option value="pilates">Pilates</option>
        <option value="ostomia">Ostomia</option>
    </select>`;
    areaListaEspecialidades.classList.remove('estilo-especialidades');
  } else {
    areaListaEspecialidades.innerHTML = `Selecione uma categoria acima`;
    areaListaEspecialidades.classList.add('estilo-especialidades');
  }
});
