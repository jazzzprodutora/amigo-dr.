<?php
    // Template Name: Outros Serviços
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- OUTROS SERVICOS -->
    <div class="outros-servicos">
        <div class="container">
            <!-- TEXTO OUTROS SERVIÇOS -->
            <div class="texto-outros-servicos">
                <h1 class="titulo">Outros Serviços<span>.</span></h1>
                <div class="texto"><?php the_field('texto-outros-servicos'); ?></div>
            </div>
           <!-- LISTA OUTROS SERVICOS -->
            <div class="lista-outros-servicos">
                <div class="container">
                    <div class="cards">
                        <!-- ... -->
                        <?php
                            $args = array (
                                'post_type' => 'servicos',//Pega os post types no array para ser mostrado nos post4
                                'categoria' =>'outros',
                                );
                                $the_query = new WP_Query ( $args );
                        ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail()?></a>
                            <h2 class="titulo"><?php the_title()?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_field('divisao'); ?></span></p>
                                <p class="a-vista">ou <?php the_field('valor-total'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>

                        <?php endwhile; else: endif; ?>


                        <!-- LOOP -->
                        <!-- <?php //if(have_rows('consultas')): while(have_rows('consultas')) : the_row(); ?>
                        <div class="item">
                            <img src="<?php the_sub_field('imagem-consulta'); ?>">
                            <h2 class="titulo"><?php the_sub_field('titulo-consulta'); ?></h2>
                            <div class="valor">
                                <p class="dividido"><span><?php the_sub_field('valor-dividido'); ?></span></p>
                                <p class="a-vista">ou <?php the_sub_field('valor-integral'); ?></p>
                            </div>
                            <div class="area-botao">
                                <a href="agende"><button class="botao botao-principal">Agendar</button></a>
                            </div>
                        </div>
                        <?php // endwhile; else : endif; ?> -->
                        <!-- ... -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>