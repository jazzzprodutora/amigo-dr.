<?php
    // Template Name: Trabalhe Conosco
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- TRABALHE CONOSCO -->
    <div class="trabalhe-conosco">
        <div class="container">
            <!-- TEXTO TRABALHE CONOSCO -->
            <div class="texto-trabalhe-conosco">
                <h1 class="titulo">Trabalhe Conosco<span>.</span></h1>
                <p class="texto"><?php the_field('texto-trabalhe-conosco'); ?></p>
            </div>

            <!-- FORMULARIO TRABALHE CONOSCO -->
            <div class="formulario">
                <div class="img-formulario">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/trabalhe-conosco/imagem-form.png">
                </div>
                <div class="campos-formulario">
                    <p class="texto-efeito">Currículo</p>
                    <h1 class="titulo">Preencha os dados</h1>
                    <p class="texto">Anexe seu currículo e boa sorte.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="168" title="Trabalhe Conosco"]' ); ?>
                </div>
            </div>
        </div>
        <!-- CARTOES -->
        <div class="cartoes">
            <div class="container">
                <!-- LOOP -->
                <?php if(have_rows('cards')): while(have_rows('cards')) : the_row(); ?>
                <div class="item">
                    <img src="<?php the_sub_field('imagem'); ?>">
                    <p><?php the_sub_field('texto'); ?></p>
                </div>
                <?php endwhile; else : endif; ?>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>