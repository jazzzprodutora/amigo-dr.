<?php
    // Template Name: Contato
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- HEADER -->
    <section class="header">
        <div class="container">
            <!-- CHAMA O CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
        </div>
    </section>


    <!-- CONTATO -->
    <div class="contato">
        <div class="container">
            <!-- TEXTO CONTATO -->
            <div class="texto-contato">
                <h1 class="titulo">Contato<span>.</span></h1>
                <div class="texto">
                    <?php the_field('texto-contato'); ?>
                </div>
            </div>

            <!-- FORMULARIO PERGUNTAS FREQUENTES -->
            <div class="formulario">
                <div class="campos-formulario">
                    <p class="texto-efeito">Entre em contato</p>
                    <h1 class="titulo">Dúvidas e sugestões</h1>
                    <p class="texto">Preencha esse formulário com seus dados e fale com a gente. Em breve, responderemos a você.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="167" title="Contato"]' ); ?>
                </div>
                <div class="img-formulario">
                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/ligue.png" id="ligue">
                        <div class="info">
                            <p class="texto">Ligue e será um prazer atender você.</p>
                            <a href="tel:<?php the_field('numero-telefone'); ?>"><?php the_field('numero-telefone'); ?></a>
                        </div>
                    </div>

                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/perguntas-frequentes/mensagem.png" id="mensagem">
                        <div class="info">
                            <p class="texto">Passe um sms, que lhe responderemos o mais rápido possível.</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.png" id="whatsapp">
                        <div class="info">
                            <p class="texto">Anote o nosso Whatsapp e fale conosco para tirar todas as suas dúvidas. Aguardamos a sua mensagem.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>
    
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>